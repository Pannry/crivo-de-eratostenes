object Form1: TForm1
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Crivo de Erat'#243'stenes'
  ClientHeight = 433
  ClientWidth = 666
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 666
    Height = 113
    Align = alTop
    TabOrder = 0
    ExplicitTop = -5
    ExplicitWidth = 656
    object lblProblema: TLabel
      Left = 16
      Top = 9
      Width = 4
      Height = 16
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 16
      Top = 87
      Width = 91
      Height = 16
      Caption = 'Insira o valor:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 182
      Top = 88
      Width = 54
      Height = 13
      Caption = '( 0 - 9999 )'
    end
    object speValue: TSpinEdit
      Left = 113
      Top = 85
      Width = 64
      Height = 22
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      TabOrder = 0
      Value = 5
      OnChange = speValueChange
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 113
    Width = 666
    Height = 320
    Align = alClient
    Caption = 'Panel2'
    TabOrder = 1
    ExplicitWidth = 656
    ExplicitHeight = 310
    object strGrd: TStringGrid
      Left = 1
      Top = 1
      Width = 664
      Height = 318
      Align = alClient
      ColCount = 10
      FixedCols = 0
      RowCount = 1
      FixedRows = 0
      ScrollBars = ssVertical
      TabOrder = 0
      ExplicitWidth = 654
      ExplicitHeight = 308
      ColWidths = (
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64)
      RowHeights = (
        24)
    end
  end
end

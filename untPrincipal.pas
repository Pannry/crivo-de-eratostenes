unit untPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Grids,
  Vcl.Samples.Spin, Vcl.ExtCtrls;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    lblProblema: TLabel;
    Label1: TLabel;
    speValue: TSpinEdit;
    Panel2: TPanel;
    strGrd: TStringGrid;
    Label2: TLabel;
    procedure speValueChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure clearRow;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}
procedure TForm1.clearRow;
var
  i : Integer;
begin
  for i := 0 to strGrd.ColCount - 1 do
    strGrd.Cols[i].Clear;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  lblProblema.Caption := 'Crivo de Erat�stenes:'+
            #10#10 + 'O Crivo de Erat�stenes d� um processo met�dico para encontrar ' +
            #10 + 'todos os n�meros primos at� um determinado valor.'
end;

procedure TForm1.speValueChange(Sender: TObject);
var
  saida : String;
  i, j, valorMaximo, raiz: Integer;
  mi, mj : Integer;
  vetor: array[1..10000] of Integer;
begin
  // Resetando a Grid
  clearRow;
  strGrd.RowCount := 1;

  // Passo 1
  valorMaximo := speValue.Value;

  // Passo 2
  raiz := Trunc( Sqrt(valorMaximo) );

  // Passo 3
  for i := 2 to valorMaximo do
    vetor[i] := i;

  // Passo 4
  for i := 2 to raiz do
    if vetor[i] = i then
    begin
      j := i+i;
      while j <= valorMaximo do
      begin
        vetor[j] := 0;
        j := j + i;
      end;
    end;

  mi := 0;
  mj := 0;

  // Mostrando o vetor final na TStringGrid
  for i := 2 to valorMaximo do
  begin
    if vetor[i] <> 0 then
    begin
      strGrd.Cells[mi, mj] := IntToStr(vetor[i]);

      mi := mi + 1;
      if mi = 10 then
      begin
        mi := 0;
        strGrd.RowCount := strGrd.RowCount + 1;
        mj := mj + 1;
      end;
    end;
  end;

end;

end.
